function gameOfLife(options) {
	var defaults = {
		width: 50,
		height: 50,
		historyLimit: -1
	}
	options = $.extend({}, defaults, options || {});
	
	var self = this;
	var $table;
	var lifeArray = [];
	var startedGame = false;
	var stepHistory = [];
	
	//init
	(function () {
		var table = document.createElement('table');
		for (var y = 0; y < options.height; y++) {
			lifeArray[y] = [];
			var tr = document.createElement('tr');
			for (var x = 0; x < options.width; x++) {
				lifeArray[y][x] = 0;
				tr.appendChild(document.createElement('td'));
			}
			table.appendChild(tr);
		}
		$table = $(table).addClass('life-field');
		$table.appendTo('body');
	})();
	
	function getStatus(pos) {
		return !!lifeArray[pos.y][pos.x];
	}
	
	function findPositionByElem(elem) {
		return {
			x: $(elem).index(),
			y: $(elem).parent().index()
		};
	}
	
	function calcNeighbours(pos) {
		//���������� ����� �������� ����� ������������ �������� ��������
		var top = !!(lifeArray[pos.y - 1] != undefined);
		var right = !!(lifeArray[pos.y][ pos.x + 1] != undefined);
		var bottom = !!(lifeArray[pos.y + 1] != undefined);
		var left = !!(lifeArray[pos.y][pos.x - 1] != undefined);
		
		var summ = 0;
		if (left && top)
			summ += lifeArray[pos.y - 1][pos.x - 1];
		if (top)
			summ += lifeArray[pos.y - 1][pos.x];
		if (top && right)
			summ += lifeArray[pos.y - 1][pos.x + 1];
		if (right)
			summ += lifeArray[pos.y][pos.x + 1];
		if (right && bottom)
			summ += lifeArray[pos.y + 1][pos.x + 1];
		if (bottom)
			summ += lifeArray[pos.y + 1][pos.x];
		if (bottom && left)
			summ += lifeArray[pos.y + 1][pos.x - 1];
		if (left)
			summ += lifeArray[pos.y][pos.x - 1];
			
		return summ;
	}
	
	function populatePoint(pos) {
		$table.find('td').eq(pos.y * options.width + +pos.x).addClass('life');
		lifeArray[pos.y][pos.x] = 1;
	}
	
	function killPoint(pos) {
		$table.find('td').eq(pos.y * options.width + +pos.x).removeClass('life');
		lifeArray[pos.y][pos.x] = 0;
	}
	
	function toggleLife(pos) {
		if (lifeArray[pos.y][pos.x])
			killPoint(pos);
		else
			populatePoint(pos);
	}
	
	
	function checkForChange(pos) {
		var neigbours = calcNeighbours({x: pos.x, y: pos.y});
		var status = getStatus({x: pos.x, y: pos.y});
		if ((status && (neigbours < 2 || neigbours > 3)) || (!status && neigbours == 3)) {
			return true;
		}
		return false;
	}
	
	function clearHistory() {
		stepHistory = [];
	}
	
	function checkHistory(currentChanges) {
		if (currentChanges.length == '') {
			$(self).trigger('noChanges', ['��� ���������']);
		} else {
			for (var i = stepHistory.length - 1; i >= 0; i--) {
				if (stepHistory[i] == currentChanges) {
					$(self).trigger('noChanges', ['������������ �����������']);
					break;
				}
			}
		}
		
		stepHistory.push(currentChanges);
		if (stepHistory.length == options.historyLimit)
			stepHistory.shift();
	}
	
	this.userToggleLife = function (elem) {					
		var pos = findPositionByElem(elem);
		toggleLife(pos);
		clearHistory();
	};
	
	this.makeStep = function () {
		var changeArray = {};
		var changeStr = [];
		for (var y = 0; y < options.height; y++) {
			for (var x = 0; x < options.width; x++) {
				if ( checkForChange({x: x, y: y}) ) {
					changeArray[y + '*' + x] = true;
					changeStr.push(y + '*' + x + '|' + lifeArray[y][x]);
				}
			}
		}
		for (var pos in  changeArray) {
			pos = pos.split('*');
			toggleLife({x: pos[1], y: pos[0]});
		}
		checkHistory(changeStr.join(','));
	}
	
	this.start = function () {
		startedGame = setInterval(self.makeStep, 500);
	}
	
	this.pause = function () {
		clearInterval(startedGame);
	}
}